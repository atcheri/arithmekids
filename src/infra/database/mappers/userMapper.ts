import { User } from "../../../domain/User";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const toDomain = (data: any): User => {
  const { id, uid, email, displayName } = data;
  return {
    id: id || uid,
    email: email,
    name: displayName || ""
  };
};

const UserMapper = {
  toDomain
};

export default UserMapper;
