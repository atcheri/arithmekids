import container from "@/ioc/container";
import { DI_TYPES } from "@/ioc/constants";
import {
  FirebaseCreateError,
  FirebaseSigninError
} from "../../firebaseDatabase";
import { Database } from "../../interfaces";
import { User } from "@/domain";
import { UserRepository } from "@/domain/User";

describe("userRepository", () => {
  describe("create function", () => {
    let dbMock: Database;
    beforeEach(() => {
      // create a snapshot so each unit test can modify
      // it without breaking other unit tests
      container.snapshot();
      dbMock = {
        create: (): Promise<User> => {
          throw new FirebaseCreateError("Test Error");
        },
        signin: (): Promise<User> => {
          throw new FirebaseSigninError("Test Error");
        },
        signout: (): Promise<boolean> => {
          throw new FirebaseSigninError("Test Error");
        }
      };
    });

    afterEach(() => {
      // Restore to last snapshot so each unit test
      // takes a clean copy of the application container
      container.restore();
    });
    it("[Error] throws an error", async () => {
      container.unbind(DI_TYPES.Database);
      container.bind<Database>(DI_TYPES.Database).toConstantValue(dbMock);
      const repo = container.get<UserRepository>(DI_TYPES.UserRepository);
      await expect(repo.create("test-email", "test-password")).rejects.toEqual(
        Error("Test Error")
      );
    });

    it("[No Error] returns a valid domain User object", async () => {
      dbMock.create = async (email: string): Promise<User> => {
        const newUser: User = {
          id: "test-id",
          email,
          name: ""
        };
        return newUser;
      };

      container.unbind(DI_TYPES.Database);
      container.bind<Database>(DI_TYPES.Database).toConstantValue(dbMock);

      const repo = container.get<UserRepository>(DI_TYPES.UserRepository);
      const expected = {
        id: "test-id",
        email: "test-mail@email.com",
        name: ""
      };

      await expect(
        repo.create("test-mail@email.com", "test-password")
      ).resolves.toMatchObject(expected);
    });
  });
});
