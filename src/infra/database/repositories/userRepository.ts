import { UserRepository } from "@/domain/User";
import { DI_TYPES } from "@/ioc/constants";
import { inject, injectable } from "inversify";
import { Database } from "../interfaces";

@injectable()
class UserRepositoryImpl implements UserRepository {
  constructor(@inject(DI_TYPES.Database) private readonly _db: Database) {}

  create = async (email: string, password: string) =>
    this._db.create(email, password);

  signin = async (email: string, password: string) =>
    this._db.signin(email, password);

  signout = (): Promise<boolean> => this._db.signout();
}

export default UserRepositoryImpl;
