import { User } from "@/domain";

export interface Database {
  create: (email: string, password: string) => Promise<User>;
  signin: (email: string, password: string) => Promise<User>;
  signout: () => Promise<boolean>;
}
