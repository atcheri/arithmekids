import firebase from "firebase/app";
require("firebase/auth");
import { inject, injectable } from "inversify";

import { DI_TYPES } from "@/ioc/constants";
import { firebaseConfig } from "@/config";
import { Database } from "./interfaces";
import UserMapper from "./mappers/userMapper";

export class FirebaseCreateError extends Error {}
export class FirebaseSigninError extends Error {}

export const init = () => {
  firebase.initializeApp(firebaseConfig);
};

@injectable()
export class FirebaseAuth {
  auth() {
    return firebase.auth();
  }
  onAuthStateChanged(fn: Function, errHandler?: Function) {
    this.auth().onAuthStateChanged(
      (user: firebase.User | null) => {
        fn(user);
      },
      (err: firebase.auth.Error) => {
        errHandler && errHandler(err);
      }
    );
  }
}

@injectable()
class FirebaseDatabase implements Database {
  @inject(DI_TYPES.FirebaseAuth) private _firebaseAuth!: FirebaseAuth;
  create = async (email: string, password: string) => {
    const {
      user
    } = await this._firebaseAuth
      .auth()
      .createUserWithEmailAndPassword(email, password);
    return UserMapper.toDomain(user);
  };

  signin = async (email: string, password: string) => {
    const { user } = await this._firebaseAuth
      .auth()
      .signInWithEmailAndPassword(email, password);
    return UserMapper.toDomain(user);
  };

  signout = async () => {
    try {
      this._firebaseAuth.auth().signOut();
      return true;
    } catch {
      return false;
    }
  };
}

export default FirebaseDatabase;
