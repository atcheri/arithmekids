import { mount } from "@vue/test-utils";
import Register from "../Register.vue";
import { RegisterUser } from "@/domain/User";
import router from "@/router";

describe("<Register />", () => {
  const createSpy = jest.fn();
  const provide = {
    Container: {
      get: () => ({
        create: async (params: RegisterUser) => {
          createSpy(params);
        }
      })
    }
  };
  describe("Given an invalid email", () => {
    it('Shows "Please enter a valid email" error message', async () => {
      const wrapper = mount(Register, {
        global: { provide, plugins: [router] }
      });
      const submitBtn = wrapper.find('button[type="submit"]');
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      const emailInput = wrapper.find('input[aria-labelledby="form-email"]');
      await emailInput.setValue("invalid-email");
      emailInput.trigger("blur");
      expect(
        wrapper.find('[data-testid="email-input-error"]').exists()
      ).toBeTruthy();
    });
  });

  describe("Given an invalid repeated password", () => {
    it('Shows "Please enter a valid email" error message', async () => {
      const wrapper = mount(Register, {
        global: { provide, plugins: [router] }
      });
      const submitBtn = wrapper.find('button[type="submit"]');
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      await wrapper
        .find('input[aria-labelledby="form-password"]')
        .setValue("test-password");
      const rPasswordInput = wrapper.find(
        'input[aria-labelledby="form-rpassword"]'
      );
      await rPasswordInput.setValue("test-wrong-password");
      rPasswordInput.trigger("blur");
      expect(
        wrapper.find('[data-testid="rpassword-input-error"]').exists()
      ).toBeTruthy();
    });
  });

  describe("Given an invalid form", () => {
    it("Disables the submit button", async () => {
      const wrapper = mount(Register, {
        global: { provide, plugins: [router] }
      });
      const submitBtn = wrapper.find('button[type="submit"]');
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      const emailInput = wrapper.find('input[aria-labelledby="form-email"]');
      await emailInput.setValue("test@test.com");
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      const passwordInput = wrapper.find(
        'input[aria-labelledby="form-password"]'
      );
      await passwordInput.setValue("test-password");
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      const rPasswordInput = wrapper.find(
        'input[aria-labelledby="form-rpassword"]'
      );
      await rPasswordInput.setValue("test-wrong-password");
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      await rPasswordInput.setValue("test-password");
      expect(submitBtn.attributes()).not.toHaveProperty("disabled");
    });
  });

  describe("Given a valid form", () => {
    it('Submits it with the email "test@test.com" and the password "test-password"', async () => {
      const wrapper = mount(Register, {
        global: { provide, plugins: [router] }
      });
      const submitBtn = wrapper.find('button[type="submit"]');
      await wrapper
        .find('input[aria-labelledby="form-email"]')
        .setValue("test@test.com");
      await wrapper
        .find('input[aria-labelledby="form-password"]')
        .setValue("test-password");
      await wrapper
        .find('input[aria-labelledby="form-rpassword"]')
        .setValue("test-password");
      expect(submitBtn.attributes()).not.toHaveProperty("disabled");
      await submitBtn.trigger("submit");
      expect(submitBtn.attributes()).toHaveProperty("disabled");
      await expect(createSpy).toHaveBeenCalledWith({
        email: "test@test.com",
        password: "test-password"
      });
      expect(submitBtn.attributes()).not.toHaveProperty("disabled");
    });
  });
});
