import { flushPromises, mount } from "@vue/test-utils";

import Logout from "../Logout.vue";
import router, { ROUTES } from "../../router";

describe("<Logout />", () => {
  describe("On component mount", () => {
    it('[ERROR] calls the signout function and router.push to "HOME" page', async () => {
      const signoutSpy = jest.fn(() => {
        throw Error("signout failed");
      });
      const provide = {
        Container: {
          get: () => ({
            signout: async () => {
              signoutSpy();
            }
          })
        }
      };
      mount(Logout, {
        global: { provide }
      });
      const pushSpy = jest.spyOn(router, "push");
      await flushPromises();
      expect(pushSpy).toHaveBeenCalledWith(ROUTES.HOME);
      expect(signoutSpy).toHaveBeenCalled();
    });

    it('[NO ERROR] calls the signout function and router.push to "LOGIN" page', async () => {
      const signoutSpy = jest.fn(async () => {
        return;
      });
      const provide = {
        Container: {
          get: () => ({
            signout: async () => {
              signoutSpy();
            }
          })
        }
      };
      mount(Logout, {
        global: { provide }
      });
      const pushSpy = jest.spyOn(router, "push");
      await flushPromises();
      expect(pushSpy).toHaveBeenCalledWith(ROUTES.LOGIN);
      expect(signoutSpy).toHaveBeenCalled();
    });
  });
});
