import { flushPromises, mount } from "@vue/test-utils";
import router, { ROUTES } from "@/router";

import Login from "../Login.vue";

describe("<Login />", () => {
  describe("Given valid inputs", () => {
    describe("When the authentication fails", () => {
      it("[ERROR] show an authentication error message", async () => {
        const provide = {
          Container: {
            get: () => ({
              signin: async () => {
                throw Error("signing-in failed");
              }
            })
          }
        };
        const wrapper = mount(Login, {
          global: { provide, plugins: [router] }
        });
        const submitBtn = wrapper.find('button[type="submit"]');
        await wrapper
          .find('input[aria-labelledby="form-email"]')
          .setValue("test@test.com");
        await wrapper
          .find('input[aria-labelledby="form-password"]')
          .setValue("test-password");
        expect(submitBtn.attributes()).not.toHaveProperty("disabled");
        await submitBtn.trigger("submit");
        await flushPromises();
        expect(
          wrapper.find('[data-testid="firebase-signin-error"]').exists()
        ).toBeTruthy();
      });
    });

    describe("When the authentication fails", () => {
      it('[NO ERROR] redirects to the "Dashboard" page', async () => {
        const signinSpy = jest.fn();
        const pushSpy = jest.fn();
        const provide = {
          Container: {
            get: () => ({
              signin: async () => signinSpy()
            })
          }
        };
        const wrapper = mount(Login, {
          global: {
            provide,
            plugins: [router],
            mocks: {
              $router: {
                push: pushSpy
              }
            }
          }
        });
        const submitBtn = wrapper.find('button[type="submit"]');
        await wrapper
          .find('input[aria-labelledby="form-email"]')
          .setValue("test@test.com");
        await wrapper
          .find('input[aria-labelledby="form-password"]')
          .setValue("test-password");
        expect(submitBtn.attributes()).not.toHaveProperty("disabled");
        await submitBtn.trigger("submit");
        await expect(signinSpy).toHaveBeenCalled();
        await expect(pushSpy).toHaveBeenCalledWith({
          name: ROUTES.DASHBOARD
        });
      });
    });
  });
});
