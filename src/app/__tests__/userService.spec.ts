// import { RegisterUser } from "@/domain";
import { User, UserRepository, UserService } from "@/domain/User";
import { DI_TYPES } from "@/ioc/constants";
import container from "@/ioc/container";

describe("UserService", () => {
  describe("create function", () => {
    let mockUserRepo: UserRepository;
    beforeEach(() => {
      // create a snapshot so each unit test can modify
      // it without breaking other unit tests
      container.snapshot();
      mockUserRepo = {
        create: (email: string, password: string): Promise<User> => {
          throw Error("Test create error");
        },
        signin: (email: string, password: string): Promise<User> => {
          throw Error("Test signin error");
        },
        signout: (): Promise<boolean> => {
          throw new Error("Test signout Error");
        }
      };
    });

    afterEach(() => {
      // Restore to last snapshot so each unit test
      // takes a clean copy of the application container
      container.restore();
    });
    it("[Error] throws an error when create fails", async () => {
      container.unbind(DI_TYPES.UserRepository);
      container
        .bind<UserRepository>(DI_TYPES.UserRepository)
        .toConstantValue(mockUserRepo);
      const service = container.get<UserService>(DI_TYPES.UserService);
      await expect(
        service.create({
          email: "test@test.com",
          password: "test-password",
          rPassword: "test-password"
        })
      ).rejects.toEqual(Error("Test create error"));
    });
    it("[No Error] returns a valid domain User on success", async () => {
      mockUserRepo.create = async (
        email: string,
        password: string
      ): Promise<User> => {
        const newUser: User = {
          id: "test-id",
          email: "test@test.com",
          name: ""
        };
        return newUser;
      };

      container.unbind(DI_TYPES.UserRepository);
      container
        .bind<UserRepository>(DI_TYPES.UserRepository)
        .toConstantValue(mockUserRepo);
      const service = container.get<UserService>(DI_TYPES.UserService);
      await expect(
        service.create({
          email: "test@test.com",
          password: "test-password",
          rPassword: "test-password"
        })
      ).resolves.toEqual({
        id: "test-id",
        email: "test@test.com",
        name: ""
      });
    });
  });
});
