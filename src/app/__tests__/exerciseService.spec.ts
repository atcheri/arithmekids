import container from "@/ioc/container";
import { ExerciseService } from "@/domain";
import { DI_TYPES } from "@/ioc/constants";

describe("ExerciseService", () => {
  describe("generate function", () => {
    describe("Given a level of 1, count of 10", () => {
      it("creates an array of 10 Exercises and all sums are in range [0, 20] ", () => {
        const level = 1;
        const count = 10;
        const sut = container.get<ExerciseService>(DI_TYPES.ExerciseService);
        const exercises = sut.generate(level, count);
        expect(exercises).toHaveLength(10);
        exercises.map(e => {
          expect(e.getSolution()).toBeLessThanOrEqual(20);
        });
      });
    });
  });
});
