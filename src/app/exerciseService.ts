import { injectable } from "inversify";

import { Exercise, ExerciseService } from "@/domain";
import { randomIntPerLevel } from "@/core/integer-functions";
import { randomOperatorPerLevel } from "@/core/string-functions";

@injectable()
class ExerciseServiceImpl implements ExerciseService {
  private generateRandomNumbers(level: number): number[] {
    return [...Array(level + 1)].map(randomIntPerLevel(level));
  }
  private generateRandomOperators(level: number): string[] {
    return [...Array(level)].map(() => randomOperatorPerLevel(level));
  }
  public generate(level: number, count: number): Exercise[] {
    return [...Array(count)].map(() => {
      const max = Math.pow(10, level) + (level === 1 ? 10 : 0);
      const instanciateExercise = () => {
        return new Exercise(
          this.generateRandomNumbers(level),
          this.generateRandomOperators(level)
        );
      };
      const isSumInRange = (exo: Exercise) =>
        exo.getSolution() > max || exo.getSolution() < 0;
      let exercise: Exercise = instanciateExercise();

      while (isSumInRange(exercise)) {
        exercise = instanciateExercise();
      }
      return exercise;
    });
  }
}

export default ExerciseServiceImpl;
