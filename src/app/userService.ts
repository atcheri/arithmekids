import { inject, injectable } from "inversify";

import { LoginUser, RegisterUser, User, UserService } from "@/domain";
import { UserRepository } from "@/domain/User";
import { DI_TYPES } from "@/ioc/constants";

@injectable()
class UserServiceImpl implements UserService {
  constructor(
    @inject(DI_TYPES.UserRepository)
    private readonly _repository: UserRepository
  ) {}
  create = async (registerUser: RegisterUser): Promise<User> => {
    return this._repository.create(registerUser.email, registerUser.password);
  };
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  signin = async (loginUser: LoginUser): Promise<User> => {
    return this._repository.signin(loginUser.email, loginUser.password);
  };
  signout = (): Promise<boolean> => {
    return this._repository.signout();
  };
}

export default UserServiceImpl;
