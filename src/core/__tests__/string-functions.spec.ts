import {
  LOW_LEVEL_OPERATORS,
  randomOperatorPerLevel
} from "../string-functions";

describe("string-functions", () => {
  describe("randomOperatorPerLevel function", () => {
    describe("Given the level of 1 or 2", () => {
      it('return either a "+" or a "-" ', () => {
        expect(LOW_LEVEL_OPERATORS).toContain(randomOperatorPerLevel(1));
        expect(LOW_LEVEL_OPERATORS).toContain(randomOperatorPerLevel(2));
      });
    });
  });
});
