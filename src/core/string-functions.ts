const OPERATORS = ["+", "-", "/", "*"];
export const LOW_LEVEL_OPERATORS = ["+", "-"];

export const strToInt = (exp: string) => parseInt(exp, 10);

export const randomOperatorPerLevel = (level: number) => {
  let operators = LOW_LEVEL_OPERATORS;
  if (level > 2) {
    operators = OPERATORS;
  }
  const operator = operators[Math.floor(Math.random() * operators.length)];
  return operator;
};
