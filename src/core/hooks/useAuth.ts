import { toRefs, reactive } from "vue";

import { FirebaseAuth } from "@/infra/database/firebaseDatabase";
import { DI_TYPES } from "@/ioc/constants";
import { User } from "@/domain";

import container from "../../ioc/container";

const useAuth = () => {
  const state = reactive<{ user: User | null }>({
    user: null
  });

  const firebaseAuth = container.get<FirebaseAuth>(DI_TYPES.FirebaseAuth);

  firebaseAuth.onAuthStateChanged((user: firebase.User) => {
    if (user) {
      state.user = {
        id: user.uid,
        name: user.displayName || "",
        email: user.email || ""
      };
    } else {
      state.user = null;
    }
  });

  return {
    ...toRefs(state)
  };
};

export default useAuth;
