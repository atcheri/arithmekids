import useAuth from "../useAuth";

import container from "@/ioc/container";
import { DI_TYPES } from "@/ioc/constants";
import { FirebaseAuth } from "@/infra/database/firebaseDatabase";

describe("useAuth", () => {
  describe("useAuth", () => {
    beforeEach(() => {
      container.snapshot();
    });

    const mockAuth: FirebaseAuth = {
      auth: () => {
        throw Error("Test auth error");
      },
      onAuthStateChanged: (fn: Function) => {
        throw Error("Test onAuthStateChanged error");
      }
    };
    afterEach(() => {
      container.restore();
    });
    it("[ERROR] returns a null user", async () => {
      mockAuth.onAuthStateChanged = (fn: Function) => {
        fn(null);
      };
      container.unbind(DI_TYPES.FirebaseAuth);
      container
        .bind<FirebaseAuth>(DI_TYPES.FirebaseAuth)
        .toConstantValue(mockAuth);
      const { user } = useAuth();
      expect(user.value).toBeNull();
    });
    it('[NO ERROR] returns a "non empty" user', async () => {
      mockAuth.onAuthStateChanged = (fn: Function) => {
        fn({
          uid: "test-id",
          displayName: "Test Man",
          email: "test@email.com"
        });
      };
      container.unbind(DI_TYPES.FirebaseAuth);
      container
        .bind<FirebaseAuth>(DI_TYPES.FirebaseAuth)
        .toConstantValue(mockAuth);
      const { user } = useAuth();
      expect(user.value).not.toBeNull();
      expect(user.value).toMatchObject({
        id: "test-id",
        email: "test@email.com",
        name: "Test Man"
      });
    });
  });
});
