export const randomIntPerLevel = (level: number) => {
  const min = 1;
  const max = Math.pow(10, level);
  return () => Math.floor(Math.random() * (max - min + 1)) + min;
};
