import { RouteRecordRaw } from "vue-router";
import { ROUTES } from ".";

const publicRoutes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: ROUTES.HOME,
    component: () => import("../views/Home.vue")
  },
  {
    path: "/login",
    name: ROUTES.LOGIN,
    component: () => import("../views/Login.vue")
  },
  {
    path: "/logout",
    name: ROUTES.LOGOUT,
    component: () => import("../views/Logout.vue")
  },
  {
    path: "/register",
    name: ROUTES.REGISTER,
    component: () => import("../views/Register.vue")
  },
  {
    path: "/:catchAll(.*)",
    name: ROUTES.PAGE_NOT_FOUND,
    component: () => import("../views/PageNotFound.vue")
  }
];

export default publicRoutes;
