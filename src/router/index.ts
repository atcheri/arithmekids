import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import container from "@/ioc/container";
import { DI_TYPES } from "@/ioc/constants";
import { FirebaseAuth } from "@/infra/database/firebaseDatabase";
import privateRoutes from "./private-routes";
import publicRoutes from "./public-routes";

export const ROUTES = Object.freeze({
  DASHBOARD: "Dashboard",
  HOME: "Home",
  LOGIN: "Login",
  LOGOUT: "Logout",
  PAGE_NOT_FOUND: "PageNotFound",
  REGISTER: "Register",
  SERIE: "Serie"
});

const routes: Array<RouteRecordRaw> = [...privateRoutes, ...publicRoutes];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);

  const firebaseAuth = container.get<FirebaseAuth>(DI_TYPES.FirebaseAuth);
  if (requiresAuth && !firebaseAuth.auth().currentUser) {
    next("/login");
  } else {
    next();
  }
});

export default router;
