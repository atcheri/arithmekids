import { RouteRecordRaw } from "vue-router";
import { ROUTES } from ".";

const privateRoutes: Array<RouteRecordRaw> = [
  {
    path: "/dashboard",
    name: ROUTES.DASHBOARD,
    component: () => import("../views/Dashboard.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/serie/:id",
    name: ROUTES.SERIE,
    component: () => import("../views/Serie.vue"),
    meta: {
      requiresAuth: true
    }
  }
];

export default privateRoutes;
