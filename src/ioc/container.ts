import { Container } from "inversify";
import "reflect-metadata";

import { DI_TYPES } from "./constants";
import { Database } from "@/infra/database/interfaces";
import FirebaseDatabase, {
  FirebaseAuth
} from "@/infra/database/firebaseDatabase";
import UserRepositoryImpl from "@/infra/database/repositories/userRepository";
import { ExerciseService, UserService } from "@/domain";
import UserServiceImpl from "@/app/userService";
import ExerciseServiceImpl from "@/app/exerciseService";

const container = new Container();
container.bind<Database>(DI_TYPES.Database).to(FirebaseDatabase);
container
  .bind<UserRepositoryImpl>(DI_TYPES.UserRepository)
  .to(UserRepositoryImpl);
container.bind<FirebaseAuth>(DI_TYPES.FirebaseAuth).to(FirebaseAuth);
container.bind<UserService>(DI_TYPES.UserService).to(UserServiceImpl);
container
  .bind<ExerciseService>(DI_TYPES.ExerciseService)
  .to(ExerciseServiceImpl);

export default container;
