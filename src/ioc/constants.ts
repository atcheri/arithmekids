export enum DI_TYPES {
  Container = "Container",
  Database = "Database",
  ExerciseService = "ExerciseService",
  FirebaseAuth = "FirebaseAuth",
  FirebaseDatabase = "FirebaseDatabase",
  UserRepository = "UserRepository",
  UserService = "UserService"
}
