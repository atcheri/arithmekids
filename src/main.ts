import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import container from "./ioc/container";
import { init as initFirebaseApp } from "@/infra/database/firebaseDatabase";
import { DI_TYPES } from "./ioc/constants";

// initializing firebase
initFirebaseApp();

const app = createApp(App);
app
  .provide(DI_TYPES.Container, container)
  .use(store)
  .use(router)
  .mount("#app");
