import { Exercise, ExerciseParametersError } from "../Exercise";

describe("Exercise class", () => {
  describe("validate function", () => {
    describe("Given empty inputs", () => {
      it('[ERROR] throws an ExerciseParametersError with message "The entered parameters are incorrect"', () => {
        expect(() => new Exercise([], [])).toThrow(ExerciseParametersError);
        expect(() => new Exercise([], [])).toThrow(
          "The entered parameters are incorrect"
        );
      });
    });
    describe("Given more operators than numbers", () => {
      it('[ERROR] throws an ExerciseParametersError with message "The number of entered arithmetic operators are incorrect"', () => {
        expect(() => new Exercise([1, 2], ["", "", ""])).toThrow(
          ExerciseParametersError
        );
        expect(() => new Exercise([1, 2], ["", "", ""])).toThrow(
          "The number of entered arithmetic operators are incorrect"
        );
      });
    });
    describe("Given invalid operators", () => {
      it('[ERROR] throws an ExerciseParametersError with message "Non authorized arithmetics operator"', () => {
        expect(
          () => new Exercise([1, 2, 3, 4], ["testing", "wrong", "operators"])
        ).toThrow(ExerciseParametersError);
        expect(
          () => new Exercise([1, 2, 3, 4], ["testing", "wrong", "operators"])
        ).toThrow("Non authorized arithmetics operator");
      });
    });
  });
  describe("Given valid parameters", () => {
    it('[NO ERROR] returns 15 for [1, 2, 3, 4, 5] and ["+", "+", "+", "+"]', () => {
      const sut = new Exercise([1, 2, 3, 4, 5], ["+", "+", "+", "+"]);
      expect(sut.formatQuestion()).toEqual("1 + 2 + 3 + 4 + 5");
      expect(sut.getSolution()).toEqual(15);
    });
    it('[NO ERROR] returns 0 for [1, 2, 3, 4, 5] and ["+", "-", "*", "/"]', () => {
      const sut = new Exercise([1, 2, 3, 4, 5], ["+", "-", "*", "/"]);
      expect(sut.formatQuestion()).toEqual("1 + 2 - 3 * 4 / 5");
      expect(sut.getSolution()).toEqual(0);
    });
    it('[NO ERROR] return -415 for [18, 7, 62, 48, 63] and ["-", "*", "+", "/"]', () => {
      const sut = new Exercise([18, 7, 62, 48, 63], ["-", "*", "+", "/"]);
      expect(sut.formatQuestion()).toEqual("18 - 7 * 62 + 48 / 63");
      expect(sut.getSolution()).toEqual(-415);
    });
    it('[NO ERROR] return 2265 for [17, 24, 8, 21, -71, 999] and ["*", "*", "-", "/", "-"]', () => {
      const sut = new Exercise(
        [17, 24, 8, 21, -71, 999],
        ["*", "*", "-", "/", "-"]
      );
      expect(sut.formatQuestion()).toEqual("17 * 24 * 8 - 21 / (-71) - 999");
      expect(sut.getSolution()).toEqual(2265);
    });
  });
});
