import { arithmeticsFromString } from "../arithmetics";

describe("arithmeticsFromString function", () => {
  it('returns 9 for "6+3"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("6+3")).toEqual(9);
  });
  it('returns 8 for "17-9"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("17-9")).toEqual(8);
  });
  it('returns 12 for "4*3"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("4*3")).toEqual(12);
  });
  it('returns 42 for "7*6"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("7*6")).toEqual(42);
  });
  it('returns 56 for "7*2*4"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("7*2*4")).toEqual(56);
  });
  it('returns 2 for "10/5"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("10/5")).toEqual(2);
  });
  it('returns 16 for "10/5*8"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("10/5*8")).toEqual(16);
  });
  it('returns 6 for "100/5/3"', () => {
    expect(arithmeticsFromString(/[+|/|*|-]/g)("100/5/3")).toEqual(6);
  });
});
