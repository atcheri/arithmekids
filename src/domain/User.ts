export type User = {
  id: string;
  name: string;
  email: string;
};

export type RegisterUser = {
  email: string;
  password: string;
  rPassword?: string;
};

export type LoginUser = {
  email: string;
  password: string;
};

export interface UserRepository {
  create: (email: string, password: string) => Promise<User>;
  signin: (email: string, password: string) => Promise<User>;
  signout: () => Promise<boolean>;
}

export class UserServiceCreateError extends Error {}
export class UserServiceSigninError extends Error {}

export interface UserService {
  create: (registerUser: RegisterUser) => Promise<User>;
  signin: (loginUser: LoginUser) => Promise<User>;
  signout: () => Promise<boolean>;
}
