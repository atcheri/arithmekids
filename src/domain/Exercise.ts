import { arithmeticsFromString, NEGATIVE } from "./arithmetics";

export interface ExerciseService {
  generate: (level: number, count: number) => Exercise[];
}

export class ExerciseParametersError extends Error {}

export class Exercise {
  constructor(
    private readonly _numbers: number[],
    private readonly _operators: string[]
  ) {
    this.validate(_numbers, _operators);
    this._numbers = _numbers;
    this._operators = _operators;
  }

  private validate(_number: number[], _operators: string[]) {
    this.validateSize(_number, _operators);
    this.validateOperators(_operators);
  }

  private validateSize(numbers: number[], operators: string[]) {
    if (!numbers.length || !operators.length) {
      throw new ExerciseParametersError("The entered parameters are incorrect");
    }
    if (operators.length !== numbers.length - 1) {
      throw new ExerciseParametersError(
        "The number of entered arithmetic operators are incorrect"
      );
    }
  }

  private validateOperators(operators: string[]) {
    const autorizedOperators = ["+", "-", "/", "*"];
    if (operators.filter(o => !autorizedOperators.includes(o)).length > 0) {
      throw new ExerciseParametersError("Non authorized arithmetics operator");
    }
  }

  private mergeNumbersAndOperators(
    numbers: number[],
    operators: string[],
    negativeSignSubstitution = (n: string) => `(${n})`
  ) {
    const tmp: string[] = [numbers[0].toString()];
    operators.map((o, i) => {
      tmp.push(o);
      let numStr = numbers[i + 1].toString();
      const num = parseInt(numStr, 10);
      if (num < 0) {
        numStr = negativeSignSubstitution(numStr);
        console.log("numStr:", numStr);
        // numStr = (-num).toString();
      }
      tmp.push(numStr);
    });
    return tmp;
  }

  public getParsableQuestion = (): string =>
    this.mergeNumbersAndOperators(this._numbers, this._operators, (n: string) =>
      n.replace("-", NEGATIVE)
    ).join("");

  public formatQuestion = (glue = " "): string => {
    return this.mergeNumbersAndOperators(this._numbers, this._operators).join(
      glue
    );
  };

  public getSolution = (): number =>
    arithmeticsFromString(/[+-]/g)(this.getParsableQuestion());
}
