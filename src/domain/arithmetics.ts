import { strToInt } from "@/core/string-functions";

export const NEGATIVE = "negative";
// const isNumeric = (test: string) => !isNaN(Number(test));

const extractParametersAndOperators = (exp: string, regex = /[+|/|*|-]/g) => {
  const operators = exp.match(regex);
  const parameters = exp.split(regex);
  return [parameters, operators];
};

const applyOperator = (operators: string[]) => (
  acc: number,
  curr: number,
  i: number
) => {
  if (i === 0) {
    return acc * curr;
  }
  switch (operators[i - 1]) {
    case "+":
      return acc + curr;
    case "-":
      return acc - curr;
    case "*":
      return acc * curr;
    case "/":
      return acc / curr;
    default:
      return acc;
  }
};

const applyOperatorsToParameters = (
  parameters: string[],
  operators: string[]
): number => {
  const result = parameters
    .map(e => {
      return e.includes(NEGATIVE)
        ? -strToInt(e.replace(NEGATIVE, ""))
        : strToInt(e);
    })
    .reduce(applyOperator(operators), 1);
  return result;
};

const splitExpAndApplyOperator = (regex: RegExp) => (exp: string): number => {
  const [parameters, operators] = extractParametersAndOperators(exp, regex);
  if (!parameters || !parameters.length) {
    throw Error("No parameters found");
  }
  if (!operators || !operators.length) {
    // throw Error("No operators found");
    return strToInt(parameters[0]);
  }
  if (operators.length === 1 && parameters.length === 2) {
    return applyOperatorsToParameters(parameters, operators);
  }

  return parameters
    .map(splitExpAndApplyOperator(/[/|*]/g))
    .reduce(applyOperator(operators), 1);
};

export const arithmeticsFromString = (regex = /[+|/|*|-]/g) => (
  exp: string
): number => {
  const solution = splitExpAndApplyOperator(regex)(exp);
  return solution < 0 ? Math.ceil(solution) : Math.floor(solution);
};
